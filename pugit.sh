# HELPER SCRIPT FOR ANGULAR APPS USING PUG 
#
# WHAT DOES THIS DO: 
#  Running this will change the component.html file to a component.pug file
#  and replace the standard <p>component works!</p> message with pug equivalent
#  then it changes the reference in the typescript file to point to the pug.
#
# WHY DO WE NEED IT:
#  it's annoying to generate a component and have it need to immediately be
#  changed in 3 places. This alleviates that tedious task.
#
# HOW TO USE IT:
#  cd into the dir holding all the parts of the new component and run
#  `pugit.sh`. The script looks through the current directory and finds the
#  relevant files/lines to change and applies the changes in place.

htmlFile=$(find . -name '*html');                                          
tsFile=$(find . -name '*component.ts');                                    
sed -i '' 's/html/pug/' $tsFile;                                           
sed -i '' 's/<p>/p /' $htmlFile;                                           
sed -i '' 's/<\/p>//' $htmlFile;                                           
mv -- "$htmlFile" "${htmlFile%.html}.pug";
echo 'HTML -> PUG ✓';

